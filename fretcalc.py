#fret calculator on python
#doesnt work on windows
import numpy as np
def getchar():
   #Returns a single character from standard input
   import tty, termios, sys
   fd = sys.stdin.fileno()
   old_settings = termios.tcgetattr(fd)
   try:
      tty.setraw(sys.stdin.fileno())
      ch = sys.stdin.read(1)
   finally:
      termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
   return ch

scale=float(input("scale length:"))
fretsno=int(input("number of frets:")) #number of frets
print("using mm? (y or n)")
mm=getchar()
if mm=='y':
	title="fret	 	mm			inches"
	factor=1/25.4
else:
	title="fret		inches		mm"
	factor=25.4
scale2=scale*factor
frets=np.array([0,scale,scale2]) #frets array
for n in range(1,fretsno+1): #for every fret
	distance=scale*1/(2**(float(n)/12)) #calculate distance to saddles
	distance2=distance*factor #convert to secondary units
	frets=np.vstack([frets,[n,distance,distance2]]) #add fret to array
print(title)
print(frets)
