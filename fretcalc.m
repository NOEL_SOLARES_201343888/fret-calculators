#fret calculator on octav
clc
scale=input("scale length:");
fretsno=input("number of frets:");
units=yes_or_no("use mm?");
if units==0
	title="fret		inches		mm";
	factor=25.4;
else
	title="fret	 	mm			inches";
	factor=1/25.4;
endif
frets=[0,scale,scale*factor];
for n=1:fretsno
	distance=scale*1/2^(n/12);
	frets=[frets;n,distance,distance*factor];
endfor
disp(title)
disp(frets)
clear
